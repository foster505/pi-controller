#!/usr/bin/env python

import sys
import os
import collections
import cherrypy
import json
#import RPi.GPIO as io

def hasher():
    return collections.defaultdict(hasher)

def main():
    p = pi_controller()
    print  p.get_config()
    sys.exit()

class pi_controller(object):
    config_file=""
    def __init__(self,config_file=""):
        if (not self.config_file):
            self.config='''{
                        "config": {
                            "temp1": {
                                "sensor": "/sys/bus/w1/devices/28-000004051438/w1_slave"
                            },
                            "switch1": {
                                "pin": 23
                            },
                            "temp_rand": {
                                "start": 50,
                                "slope": 60,
                                "duration": 1
                            }
                        }
                    }'''
            self.config = json.loads(self.config)

    def get_config(self):
#        print json.dumps(self.config,indent=3)
        return self.config

    def get_temp(self,name="temp1"):
        valid = 0		
        temp = 0
        proc = subprocess.Popen(['cat',sensor],stdout=subprocess.PIPE)
        for line in iter(proc.stdout.readline,''):
            line = line.rstrip()
            if line.rfind(":") !=-1:#validation line
                val = line.split(" ")[-1]
                #print "val" + val
                if (val=="YES"):
                    valid = 1
            if line.rfind("t=") !=-1:#temperature line
                val = line.split("t=")[-1]
                #print "temp" + val
                temp = float(val)/float(1000)
                temp=self.cToF(temp)
            return {'valid':valid, 'temp':temp}


    def set_switch(self,**kwargs):
        for s in kwargs.keys():
            if(kwargs[s].type()==bool and kwargs[s]==True):
                self.config[s]=1
                
        '''

            io.setmode(io.BCM)
            pin = 23
            io.setwarnings(False)
            io.setup(pin, io.OUT)
            io.output(pin, True) #turns on
            '''    
    def cToF(c):
        f = c* float(9)/float(5) + float(32)
	return round(f,3)


if __name__ == "__main__":
    main()




def fToC(f):
    c = (float(5)/float(9))*(f - float(32))
    return round(c,3)

