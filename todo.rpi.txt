-rpi webservice in docker for retrieving temp and setting switches
-webservice in docker for maintaining temp, logging temp, setting thresholds
-rpi image

1)   rpi controller class
   methods:       
   get temp1
   get temp_rand(start,sd,slope(deg/minute),duration minute,)
   switch {"switch1"=True,"switch2"=False} returns {"switch1"=True,"switch2"=False}
   
   rpi controller webservice Class cherrypy
   config:{"host":127.0.0.1,port:"8081",controller:new_controller}
   /temp -> returns {timestamp, temp degF}
   /switch?1=off,2=off  ->sets and returns all switch status


   

2) control interface service
      config:{"host":127.0.0.1,port:"8081",db=sqlite,min=sqlite.thresh,max=sqlite.thresh, default minmax=50/60, refresh=10s, lag=5s}
   threshold test
   flip switch
   monitor polling with sqlite logging (time,temp), thresholds
   

3) rpi image
   1) rasp image
      curl -O https://downloads.raspberrypi.org/raspbian_latest
   	   ssh pi@raspberrypi.local password = raspberry change with psswd
      reduce gpu memory      /boot/config.txt -> gpu_mem=16  
   1)install docker
   	     curl -sSL get.docker.com | sh
	     sudo systemctl enable docker #auto launch
	      sudo usermod -aG docker pi

   2) basic docker
      FROM resin/rpi-raspbian:jessie-20160831
      RUN apt-get update && \
          apt-get -qy install curl ca-certificates
	  CMD ["curl", "https://docker.com"]

   3) GPIO docker
      FROM resin/rpi-raspbian:jessie-20160831

RUN apt-get -q update && \
    apt-get -qy install \
            python python-pip \
	            python-dev python-pip gcc make
		    RUN pip install rpi.gpio


   2)get controller image and run

   rpi docker image


http://blog.alexellis.io/getting-started-with-docker-on-raspberry-pi/

